# docker-compose-bluewalker-telegraf

Ruuvitag measurement reader setup using:
  * [bluewalker](https://gitlab.com/jtaimisto/bluewalker)
  * [telegraf](https://www.influxdata.com/time-series-platform/telegraf/)

## .env variables
```
# Bluewalker
BT_DEVICE=hci1 # Defaults to hci0

# INFLUXDB
INFLUX_DB=
INFLUX_USER=
INFLUX_PASSWORD=
INFLUX_HOST=

# MQTT
MQTT_SERVER=
MQTT_USER=
MQTT_PASSWORD=

```

By default it reports measurements via MQTT. See telegraf/telegraf.conf for more output options

